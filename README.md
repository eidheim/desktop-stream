A small program that streams screenshots to web browsers through WebSocket.

Currently, the images are streamed directly without any streaming compression implemented.
Screenshots are additionally created and resized using `sh` instead of a library (mainly because of
missing ports of the screen grab part of frequently used libraries (for instance Gtk+, ImageMagick) to OS X). 

Depends on ImageMagick, Boost, OpenSSL, and the linked repositories.

```sh
git clone --recursive https://gitlab.com/eidheim/desktop-stream
mkdir desktop-stream/build
cd desktop-stream/build
cmake ..
# You might want to change the const variable values before compilation (see main.cpp)
make
./desktop_stream
```

After running desktop-stream, open [http://localhost:8080](http://localhost:8080).
